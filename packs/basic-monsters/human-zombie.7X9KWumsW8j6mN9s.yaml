_id: 7X9KWumsW8j6mN9s
_key: '!actors!7X9KWumsW8j6mN9s'
_stats:
  coreVersion: '12.331'
effects:
  - _id: 7AODaqBgSrqmNPlI
    _key: '!actors.effects!7X9KWumsW8j6mN9s.7AODaqBgSrqmNPlI'
    _stats:
      coreVersion: '12.331'
    duration:
      startTime: 0
    flags:
      pf1:
        autoDelete: true
    img: systems/pf1/icons/conditions/staggered.png
    name: Staggered
    sort: 0
    statuses:
      - staggered
    tint: '#ffffff'
    type: base
img: systems/pf1/icons/skills/shadow_04.jpg
items:
  - _id: FpyMLt1LPY18Ji76
    _key: '!actors.items!7X9KWumsW8j6mN9s.FpyMLt1LPY18Ji76'
    _stats:
      compendiumSource: Compendium.pf1.racialhd.Item.mp1Zmbx0OAzSW4oW
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/affliction_07.jpg
    name: Undead
    system:
      bab: med
      classSkills:
        clm: true
        dis: true
        fly: true
        int: true
        kar: true
        kre: true
        per: true
        sen: true
        spl: true
        ste: true
      description:
        value: >-
          <p>Undead are once-living creatures animated by spiritual or
          supernatural forces.</p>

          <h2>Features</h2>

          <p>An undead creature has the following features.</p>

          <ul>

          <li>d8 Hit Die.</li>

          <li>Base attack bonus equal to 3/4 total Hit Dice (medium
          progression).</li>

          <li>Good <em>Will</em> Saves.</li>

          <li>Skill points equal to 4 + Int modifier (minimum 1) per Hit Die.
          Many undead, however, are mindless and gain no skill points or feats.
          The following are class skills for undead: <em>Climb</em>,
          <em>Disguise</em>, <em>Fly</em>, <em>Intimidate</em>,
          <em>Knowledge</em> (arcane), <em>Knowledge</em> (religion),
          <em>Perception</em>, <em>Sense Motive</em>, <em>Spellcraft</em>, and
          <em>Stealth</em>.</li>

          </ul>

          <h2>Traits</h2>

          <p>An undead creature possesses the following traits (unless otherwise
          noted in a creature’s entry).</p>

          <ul>

          <li>No Constitution score. Undead use their <em>Charisma</em> score in
          place of their Constitution score when calculating hit points,
          <em>Fortitude</em> saves, and any special ability that relies on
          Constitution (such as when calculating a breath weapon’s DC).</li>

          <li><em>Darkvision</em> 60 feet.</li>

          <li>Immunity to all mind-affecting effects (charms, compulsions,
          morale effects, patterns, and phantasms).</li>

          <li>Immunity to death effects, disease, paralysis, poison, sleep
          effects, and stunning.</li>

          <li>Not subject to nonlethal damage, <em>ability drain</em>, or energy
          drain. Immune to <em>damage</em> to its physical ability scores
          (Constitution, Dexterity, and <em>Strength</em>), as well as to
          exhaustion and fatigue effects.</li>

          <li>Cannot heal damage on its own if it has no <em>Intelligence</em>
          score, although it can be healed. Negative energy (such as an inflict
          spell) can heal undead creatures. The fast healing special quality
          works regardless of the creature’s <em>Intelligence</em> score.</li>

          <li>Immunity to any effect that requires a <em>Fortitude</em> save
          (unless the effect also works on objects or is harmless).</li>

          <li>Not at risk of death from massive damage, but is immediately
          destroyed when reduced to 0 hit points.</li>

          <li>Not affected by <em>raise dead</em> and <em>reincarnate</em>
          spells or abilities. Resurrection and <em>true resurrection</em> can
          affect undead creatures. These spells turn undead creatures back into
          the living creatures they were before becoming undead.</li>

          <li>Proficient with its natural weapons, all simple weapons, and any
          weapons mentioned in its entry.</li>

          <li>Proficient with whatever type of armor (light, medium, or heavy)
          it is described as wearing, as well as all lighter types. Undead not
          indicated as wearing armor are not proficient with armor. Undead are
          proficient with shields if they are proficient with any form of
          armor.</li>

          <li>Undead do not breathe, eat, or sleep.</li>

          </ul>
      hp: 9
      level: 2
      savingThrows:
        will:
          value: high
      skillsPerLevel: 4
      subType: racial
      tag: undead
    type: class
  - _id: LroIG0PaCzAjEKZC
    _key: '!actors.items!7X9KWumsW8j6mN9s.LroIG0PaCzAjEKZC'
    _stats:
      compendiumSource: Compendium.pf1.feats.Item.8snLqsJN4LLL00Nq
      coreVersion: '12.331'
    img: systems/pf1/icons/feats/toughness.jpg
    name: Toughness
    system:
      changes:
        - _id: 8QgcqYEZ
          formula: max(3, @attributes.hd.total)
          target: mhp
          type: untyped
      description:
        value: >-
          <i>You have enhanced physical
          stamina.</i><p><strong>Benefits</strong>: You gain +3 hit points. For
          every Hit Die you possess beyond 3, you gain an additional +1 hit
          point. If you have more than 3 Hit Dice, you gain +1 hit points
          whenever you gain a Hit Die (such as when you gain a level).</p>
      sources:
        - id: PZO1110
          pages: 116, 135
      tags:
        - General
        - Defensive
    type: feat
  - _id: 09RM7uRLwLYVnFBf
    _key: '!actors.items!7X9KWumsW8j6mN9s.09RM7uRLwLYVnFBf'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.GrbQIXcmp5VXxYA7
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/fire_07.jpg
    name: Slam
    system:
      actions:
        - _id: lyysy62a9hqkvo9c
          ability:
            attack: str
            damage: str
            damageMult: 1.5
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          damage:
            parts:
              - formula: sizeRoll(1, 4, @size + 1)
                types:
                  - bludgeoning
          name: Attack
          range:
            units: melee
          tag: slam
      baseTypes:
        - Slam
      description:
        value: >-
          <p>A primary natural weapon attack where you slam with a part of your
          body.</p><p>It deals bludgeoning damage.</p><hr /><p>For general rules
          on natural attacks, see:
          @UUID[Compendium.pf1.pf1e-rules.nBnUY0koBzXqoEft.JournalEntryPage.8zivKFrhTVkbtjm3]{Natural
          Attacks} (UMR).</p>
      proficient: true
      subType: natural
      weaponGroups:
        - natural
    type: attack
  - _id: bIhpg8iU4lDQRwuY
    _key: '!actors.items!7X9KWumsW8j6mN9s.bIhpg8iU4lDQRwuY'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/conditions/staggered.png
    name: Staggered
    system:
      abilityType: ex
      description:
        value: >-
          <p>Zombies have poor reflexes and can only perform a single move
          action or standard action each round. A zombie can move up to its
          speed and attack in the same round as a charge
          action.</p><p><strong>Use:</strong> set active the <em>Staggered</em>
          condition if not active already.</p>
      sources:
        - id: PZO1112
          pages: '288'
      subType: misc
    type: feat
  - _id: VkgOh6incarpQUYz
    _key: '!actors.items!7X9KWumsW8j6mN9s.VkgOh6incarpQUYz'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.abPQPu23CmkfWudq
      coreVersion: '12.331'
    img: icons/creatures/eyes/humanoid-single-blind.webp
    name: Darkvision
    system:
      abilityType: ex
      description:
        value: >-
          <p>A creature with darkvision can see in total darkness, usually to a
          range of 60 feet. Within this range, the creature can see as clearly
          as a sighted creature could see in an area of bright light. Darkvision
          is black and white only but otherwise like normal
          sight.</p><p><em>Format:</em> darkvision 60
          ft.</p><p><em>Location:</em> Senses.</p>
      sources:
        - id: PZO1133
          pages: '292'
        - id: PZO1127
          pages: '292'
        - id: PZO1137
          pages: '292'
      subType: misc
    type: feat
  - _id: g9E5KdA4XNmxMfvh
    _key: '!actors.items!7X9KWumsW8j6mN9s.g9E5KdA4XNmxMfvh'
    _stats:
      compendiumSource: Compendium.pf1.monster-abilities.Item.kcCRPAHsrWouTbij
      coreVersion: '12.331'
    img: icons/magic/death/hand-undead-skeleton-fire-green.webp
    name: Undead Traits
    system:
      abilityType: ex
      description:
        value: >-
          <p>Undead are immune to death effects, disease, mind-affecting effects
          (charms, compulsions, morale effects, patterns, and phantasms),
          paralysis, poison, sleep, stun, and any effect that requires a
          Fortitude save (unless the effect also works on objects or is
          harmless). Undead are not subject to ability drain, energy drain, or
          nonlethal damage. Undead are immune to damage or penalties to their
          physical ability scores (Strength, Dexterity, and Constitution), as
          well as to fatigue and exhaustion effects. Undead are not at risk of
          death from massive damage.</p><p><em>Format:</em> undead
          traits</p><p><em>Location:</em> Immune.</p>
      sources:
        - id: PZO1112
          pages: '305'
        - id: PZO1116
          pages: '303'
        - id: PZO1120
          pages: '300'
        - id: PZO1127
          pages: '301'
        - id: PZO1133
          pages: '301'
        - id: PZO1137
          pages: '300'
      subType: misc
    type: feat
name: Human Zombie
prototypeToken:
  sight:
    enabled: true
  texture:
    src: systems/pf1/icons/skills/shadow_04.jpg
system:
  abilities:
    con:
      value: null
    int:
      value: null
    str:
      value: 17
  attributes:
    naturalAC: 2
  details:
    alignment: ne
    biography:
      value: >-
        <p>Zombies are the animated corpses of dead creatures, forced into foul
        unlife via necromantic magic like animate dead. While the most commonly
        encountered zombies are slow and tough, others possess a variety of
        traits, allowing them to spread disease or move with increased
        speed.</p><p>Zombies are unthinking automatons, and can do little more
        than follow orders. When left unattended, zombies tend to mill about in
        search of living creatures to slaughter and devour. Zombies attack until
        destroyed, having no regard for their own safety.</p><p>Although capable
        of following orders, zombies are more often unleashed into an area with
        no command other than to kill living creatures. As a result, zombies are
        often encountered in packs, wandering around places the living frequent,
        looking for victims. Most zombies are created using animate dead. Such
        zombies are always of the standard type, unless the creator also casts
        haste or remove paralysis to create fast zombies, or contagion to create
        plague zombies.</p>
    bonusFeatFormula: '1'
    cr:
      base: 0.5
    notes:
      value: <p><strong>Source</strong> <em>Pathfinder RPG Bestiary pg. 288</em></p>
  traits:
    ci:
      - deathEffects
      - disease
      - energyDrain
      - exhausted
      - fatigue
      - mindAffecting
      - paralyze
      - poison
      - sleep
      - stun
    di:
      - nonlethal
    dr:
      value:
        - amount: 5
          operator: true
          types:
            - slashing
            - ''
    senses:
      dv:
        value: 60
type: npc
