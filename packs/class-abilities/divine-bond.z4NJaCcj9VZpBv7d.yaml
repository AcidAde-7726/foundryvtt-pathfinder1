_id: z4NJaCcj9VZpBv7d
_key: '!items!z4NJaCcj9VZpBv7d'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/feats/animal-affinity.jpg
name: Divine Bond
system:
  abilityType: sp
  associations:
    classes:
      - Paladin
  description:
    value: >-
      <p>Upon reaching 5th level, a paladin forms a divine bond with her god.
      This bond can take one of two forms. Once the form is chosen, it cannot be
      changed.</p>

      <p>The first type of bond allows the paladin to enhance her weapon as a
      standard action by calling upon the aid of a celestial spirit for 1 minute
      per paladin level. When called, the spirit causes the weapon to shed light
      as a torch. At 5th level, this spirit grants the weapon a +1 enhancement
      bonus. For every three levels beyond 5th, the weapon gains another +1
      enhancement bonus, to a maximum of +6 at 20th level. These bonuses can be
      added to the weapon, stacking with existing weapon bonuses to a maximum of
      +5, or they can be used to add any of the following weapon properties:
      <em>axiomatic</em>, <em>brilliant energy</em>, <em>defending</em>,
      <em>disruption</em>, <em>flaming</em>, <em>flaming burst</em>,
      <em>holy</em>, <em>keen</em>, <em>merciful</em>, and <em>speed</em>.
      Adding these properties consumes an amount of bonus equal to the
      property’s cost (see Table: Melee Weapon Special Abilities). These bonuses
      are added to any properties the weapon already has, but duplicate
      abilities do not stack. If the weapon is not magical, at least a +1
      enhancement bonus must be added before any other properties can be added.
      The bonus and properties granted by the spirit are determined when the
      spirit is called and cannot be changed until the spirit is called again.
      The celestial spirit imparts no bonuses if the weapon is held by anyone
      other than the paladin but resumes giving bonuses if returned to the
      paladin. These bonuses apply to only one end of a double weapon. A paladin
      can use this ability once per day at 5th level, and one additional time
      per day for every four levels beyond 5th, to a total of four times per day
      at 17th level.</p>

      <p>If a weapon bonded with a celestial spirit is destroyed, the paladin
      loses the use of this ability for 30 days, or until she gains a level,
      whichever comes first. During this 30-day period, the paladin takes a –1
      penalty on attack and weapon damage rolls.</p>

      <p>The second type of bond allows a paladin to gain the service of an
      unusually intelligent, strong, and loyal steed to serve her in her crusade
      against evil. This mount is usually a heavy horse (for a Medium paladin)
      or a pony (for a Small paladin), although more exotic mounts, such as a
      boar, camel, or dog are also suitable. This mount functions as a druid’s
      <em>animal companion</em>, using the paladin’s level as her effective
      druid level. Bonded mounts have an <em>Intelligence</em> of at least
      6.</p>

      <p>In addition to horses, a paladin may select (at the GM’s discretion)
      some less ordinary creatures.</p>

      <p>A Medium paladin can select an elk, giant seahorse, giraffe, yak, or
      zebra as a bonded mount.</p>

      <p>A Small paladin can select an antelope, capybara, eohippus, giant
      weasel, ram, reindeer, stag, or wolfdog as a bonded mount.</p>

      <p>The auspice, bodyguard, racer, and totem guide archetypes are all
      particularly appropriate for a paladin’s bonded mount.</p>

      <p>Once per day, as a full-round action, a paladin may magically call her
      mount to her side. This ability is the equivalent of a spell of a level
      equal to one-third the paladin’s level. The mount immediately appears
      adjacent to the paladin. A paladin can use this ability once per day at
      5th level, and one additional time per day for every 4 levels thereafter,
      for a total of four times per day at 17th level.</p>

      <p>At 11th level, the mount gains the <em>celestial creature simple
      template</em> and becomes a <em>magical beast</em> for the purposes of
      determining which spells affect it.</p>

      <p>At 15th level, a paladin’s mount gains <em>spell resistance</em> equal
      to the paladin’s level + 11.</p>

      <p>Should the paladin’s mount die, the paladin may not summon another
      mount for 30 days or until she gains a paladin level, whichever comes
      first. During this 30-day period, the paladin takes a –1 penalty on attack
      and weapon damage rolls.</p>
  subType: classFeat
type: feat
