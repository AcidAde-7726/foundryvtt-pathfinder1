// SPDX-FileCopyrightText: 2021 Johannes Loher
// SPDX-FileCopyrightText: 2022 Ethaks
//
// SPDX-License-Identifier: MIT

import path from "node:path";
import url from "node:url";

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import fsp from "node:fs/promises";
import fs from "node:fs";

import { mkdir, readJSONSync } from "./lib.mjs";

import { FOUNDRY_CONFIG } from "./foundry-config.mjs";

const __filename = url.fileURLToPath(import.meta.url);

/**
 * The directory that will be linked in Foundry's data directory.
 *
 * @type {string}
 */
const DIST_DIRECTORY = path.resolve("dist");
console.log("Build directory:", DIST_DIRECTORY);

/**
 * The type of Foundry package of this project
 *
 * @type {"module" | "system"}
 */
const PACKAGE_TYPE = "system";

// Command handling

if (process.argv[1] === __filename) {
  yargs(hideBin(process.argv))
    .demandCommand(1, 1)
    .command({
      command: "dist",
      describe: "Link dist directory to Foundry's Data",
      builder: (yargsBuilder) => {
        return yargsBuilder.option("clean", { describe: "Remove link to Foundry's Data" });
      },
      handler: async (argv) => {
        await linkPackage(argv.clean);
      },
    })
    .parse();
}

/**
 * Get the data path of Foundry VTT based on what is configured in {@link FOUNDRY_CONFIG}.
 */
function getDataPath() {
  if (FOUNDRY_CONFIG?.dataPath) {
    if (!fs.existsSync(FOUNDRY_CONFIG.dataPath)) {
      throw new Error("User data path invalid, no Data directory found");
    }
    return FOUNDRY_CONFIG.dataPath;
  } else {
    throw new Error(`No user data path defined in foundryconfig.json`);
  }
}

/**
 * Get the name/id of the package based on its manifest file.
 */
function getPackageName() {
  if (!fs.existsSync(path.join("public", `${PACKAGE_TYPE}.json`))) {
    throw new Error(`Could not find ${PACKAGE_TYPE}.json`);
  }
  const manifest = readJSONSync(path.join("public", `${PACKAGE_TYPE}.json`));
  const name = manifest.id || manifest.name;
  if (!name) {
    throw new Error(`Could not find name in ${PACKAGE_TYPE}.json`);
  }
  return name;
}

/**
 * Link the built package to the user data folder.
 *
 * @param {boolean} clean Whether to remove the link instead of creating it
 */
async function linkPackage(clean) {
  const linkDirectory = path.resolve(getDataPath(), "Data", `${PACKAGE_TYPE}s`, getPackageName());
  console.log("Link target:", linkDirectory);

  if (clean) {
    console.log(`Removing link to built package at ${linkDirectory}.`);
    await fsp.remove(linkDirectory);
  } else if (!fs.existsSync(linkDirectory)) {
    console.log(`Linking built package to ${linkDirectory}.`);
    await mkdir(path.resolve(linkDirectory, ".."));
    await fsp.symlink(DIST_DIRECTORY, linkDirectory);
  } else {
    console.warn("Target directory or link already exists.");
  }
}
