/**
 * Compacting Mixin
 *
 * Provides framework for automatic data pruning.
 *
 * @param {T} Base - Base datamodel
 * @returns {T} - Enriched class
 */
export const CompactingMixin = (Base) =>
  class extends Base {
    /** @override */
    toObject(source = true, clean = true) {
      const data = super.toObject(source);

      if (clean) this.constructor.pruneData(data);

      return data;
    }

    /**
     * Prune data
     *
     * @abstract
     * @protected
     * @param {object} data - Source data to prune
     * @returns {void}
     */
    // eslint-disable-next-line no-unused-vars
    static pruneData(data) {}
  };
