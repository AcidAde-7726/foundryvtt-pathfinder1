export {};

type DurationEndEvent = keyof typeof pf1.config.durationEndEvents;

declare module "./ae-base.mjs" {
  interface AEBaseModel {
    /**
     * End timing
     */
    end?: DurationEndEvent;
    /**
     * Initiative count
     */
    initiative?: number;
    /**
     * Level
     *
     * Unused
     */
    level?: number;
  }
}
