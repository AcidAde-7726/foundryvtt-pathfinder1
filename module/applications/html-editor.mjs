const { DocumentSheetV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * HTML Editor
 *
 * Generic HTML text editor.
 *
 * @group Applications
 */
export class HTMLEditor extends HandlebarsApplicationMixin(DocumentSheetV2) {
  /** @override */
  _initializeApplicationOptions(options) {
    options = super._initializeApplicationOptions(options);

    options.uniqueId += `-${options.path.replaceAll(".", "-")}`;

    return options;
  }

  /** @override */
  static DEFAULT_OPTIONS = {
    tag: "form",
    classes: ["pf1-v2", "html-editor"],
    window: {
      minimizable: false,
      resizable: true,
    },
    position: {
      height: 520,
      width: 580,
    },
    sheetConfig: false,
    form: {
      closeOnSubmit: true,
      submitOnChange: true,
    },
  };

  /** @override */
  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/html-editor.hbs",
    },
  };

  /** @override */
  get title() {
    return this.options.window.title || super.title;
  }

  /** @override */
  _prepareContext() {
    return {
      uuid: this.document.uuid,
      content: foundry.utils.getProperty(this.document, this.options.path),
      path: this.options.path,
      editable: this.isEditable,
      field: new foundry.data.fields.HTMLField(),
    };
  }

  /** @override */
  _processSubmitData(event, form, submitData) {
    if (this.options.callback) return this.options.callback(submitData);
    else this.document.update(submitData);
  }
}
