/**
 * Base item
 *
 * From which all other item documents inherit from.
 */
export class ItemBasePF extends Item {
  /**
   * Add default artwork.
   *
   * @see {@link pf1.config.defaultIcons.items}
   *
   * @internal
   * @override
   * @param {object} [itemData] - Item data
   * @returns {{img:string}} - Default image
   */
  static getDefaultArtwork(itemData) {
    const result = super.getDefaultArtwork(itemData);
    const image = pf1.config.defaultIcons.items[itemData?.type];
    if (image) result.img = image;
    return result;
  }

  /**
   * Item create dialog.
   *
   * @override
   * @param {object} data Initial form data
   * @param {object} [context] Additional options.
   * @param {Actor|null} [context.parent=null] Parent parameter passed to Item.create() options
   * @param {string|null} [context.pack=null] Pack ID parameter passed to Item.create() options
   * @param {Array<string>} [context.types] Item types to limit creation choices to.
   * @param {object} [context.options] Dialog context options.
   * @returns {Promise<Item|null>}
   *
   * Synchronized with Foundry VTT v12.331
   */
  static async createDialog(data = {}, { parent = null, pack = null, types, ...options } = {}) {
    // Transform AppV1 position to AppV2
    const { width, left, top } = options;
    const position = { width, left, top };
    return pf1.applications.item.CreateDialog.waitPrompt({ data, parent, pack, types, position, options });
  }

  /**
   * Override to provide naming by subType potential.
   *
   * @override
   * @param {object} context - Context where the name would be used
   * @param {string} [context.type] - Type
   * @param {string} [context.subType] - Document subtype. Nonstandard option.
   * @param {Document|null} [context.parent] - Parent document
   * @param {string|null} [context.pack] - Pack this would be within
   * @returns {string}
   * Synced with Foundry v12.331
   */
  static defaultName({ type, subType, parent, pack } = {}) {
    const documentName = this.metadata.name;
    let collection;
    if (parent) collection = parent.getEmbeddedCollection(documentName);
    else if (pack) collection = game.packs.get(pack);
    else collection = game.collections.get(documentName);
    const takenNames = new Set();
    for (const document of collection) takenNames.add(document.name);
    let baseNameKey = this.metadata.label;
    if (type && this.hasTypeData) {
      let typeNameKey;
      if (subType && CONFIG.Item.documentClasses[type]?.system?.subtypeName) {
        const key = `PF1.Subtypes.Item.${type}.${subType}.Single`;
        if (game.i18n.has(key)) typeNameKey = key;
      }
      typeNameKey ||= CONFIG[documentName].typeLabels?.[type];
      if (typeNameKey && game.i18n.has(typeNameKey)) baseNameKey = typeNameKey;
    }
    const baseName = game.i18n.localize(baseNameKey);
    let name = baseName;
    let index = 1;
    while (takenNames.has(name)) name = `${baseName} (${++index})`;
    return name;
  }

  /**
   * On-Create Operation
   *
   * Post-create processing so awaiting the original operation has all secondary updates completed when it returns.
   *
   * @override
   * @param {Document[]} documents - Documents
   * @param {*} operation  - Operations and context data
   * @param {User} user - Triggering user
   */
  static async _onCreateOperation(documents, operation, user) {
    await super._onCreateOperation(documents, operation, user);

    if (!user.isSelf) return;

    // Operation parent can be ActorDelta, so we try to get the synthetic actor if it exists.
    /** @type {Actor} */
    const actor = operation.parent?.syntheticActor ?? operation.parent;
    if (!(actor instanceof Actor)) return;

    // Create spellbook if the class has spellcasting defined
    const promises = [];

    for (const item of documents) {
      // Creation data
      const data = operation.data.find((i) => i._id === item.id);
      if (!data) continue; // Shouldn't happen

      // Class added
      switch (item.type) {
        case "class": {
          // Has casting
          if (item.system.casting?.type) {
            // Create spellbook entry
            const bookData = { ...item.system.casting, class: item.system.tag };
            const p = actor.createSpellbook(bookData);
            promises.push(p);
          }

          // Adjust associations if any exist
          const level = item.system.level ?? 0;
          if (level > 0) {
            const p = item._onLevelChange(0, level, { event: "create" });
            promises.push(p);
          }
          break;
        }
        case "buff": {
          if (item.isActive) {
            const p = item._updateTrackingEffect(data);
            promises.push(p);
          }
          break;
        }
        case "race": {
          // Update owning actor speed to match racial speed.
          // TODO: Make this derived data on the actor instead, eliminating the update.
          if (item.system.speeds) {
            const speedUpdates = {};
            for (const key of pf1.const.movementKeys) {
              speedUpdates[key] = { base: item.system.speeds[key] ?? null };
            }
            if (speedUpdates.fly) {
              speedUpdates.fly.maneuverability = item.system.speeds.flyManeuverability || "average";
            }
            const p = actor.update({ "system.attributes.speed": speedUpdates });
            promises.push(p);
          }
          break;
        }
      }
    }

    // Supplements
    if (operation._pf1NoSupplements !== true) {
      const p = this._createSupplements(documents, actor);
      promises.push(p);
    }

    await Promise.all(promises);
  }

  /**
   * On-Update Operation
   *
   * Post-update processing so awaiting the original operation has all secondary updates completed when it returns.
   *
   * @override
   * @param {Document[]} documents - Documents
   * @param {*} operation  - Operations and context data
   * @param {User} user - Triggering user
   */
  static async _onUpdateOperation(documents, operation, user) {
    await super._onUpdateOperation(documents, operation, user);

    if (!user.isSelf) return;

    // Operation parent can be ActorDelta, so we try to get the synthetic actor if it exists.
    /** @type {Actor} */
    const actor = operation.parent?.syntheticActor ?? operation.parent;
    if (!(actor instanceof Actor)) return;

    const promises = [];

    for (const item of documents) {
      const changed = operation.updates.find((i) => i._id === item.id);
      if (!changed) continue; // Shouldn't happen

      switch (item.type) {
        case "class": {
          // Update class associations if level changed
          const oldLevel = operation.pf1?.item?.[item.id]?.oldLevel ?? 0;
          const newLevel = changed.system?.level ?? 0;
          if (newLevel !== oldLevel) {
            const p = item._onLevelChange(oldLevel, newLevel, { event: "update" });
            promises.push(p);
          }
          break;
        }
        case "buff": {
          const p = item._updateTrackingEffect(changed);
          promises.push(p);
          break;
        }
        case "race": {
          // Change actor size if the old size is same as old race size.
          if (operation._pf1SizeChanged) {
            const p = actor.update({ "system.traits.size": item.system.size });
            promises.push(p);
          }
          break;
        }
      }
    }

    await Promise.all(promises);
  }

  /**
   * On-Delete Operation
   *
   * Post-delete processing so awaiting the original operation has all secondary updates completed when it returns.
   *
   * @override
   * @param {Document[]} documents - Documents
   * @param {*} operation  - Operations and context data
   * @param {User} user - Triggering user
   */
  static async _onDeleteOperation(documents, operation, user) {
    await super._onDeleteOperation(documents, operation, user);

    if (!user.isSelf) return;

    // Operation parent can be ActorDelta, so we try to get the synthetic actor if it exists.
    /** @type {Actor} */
    const actor = operation.parent?.syntheticActor ?? operation.parent;
    if (!(actor instanceof Actor)) return;

    const promises = [];

    for (const item of documents) {
      switch (item.type) {
        case "class": {
          // Adjust associations if any exist
          const prevLevel = item.system.level ?? 0;
          if (prevLevel > 0) {
            const p = item._onLevelChange(prevLevel, 0, { event: "delete" });
            promises.push(p);
          }

          // Disable spellbook associated with this class, if it has spellcasting defined
          const tag = item.system.tag;
          if (!tag || !item.system.casting?.type) continue;

          const books = actor.system.attributes.spells.spellbooks ?? {};
          const usedBook = Object.entries(books).find(([_, book]) => !!book.class && book.class === tag);
          if (usedBook === undefined) continue;

          const [bookId, book] = usedBook;
          if (!book.inUse) continue;

          const p = actor.update({ [`system.attributes.spells.spellbooks.${bookId}.inUse`]: false });
          promises.push(p);
          break;
        }
        case "race": {
          if (actor.itemTypes.race.length === 0) {
            // Reset some race dependant details
            // TODO: Make this derived data
            const p = actor.update({
              "system.attributes.speed": {
                "land.base": 30,
                "fly.base": 0,
                "fly.maneuverability": "average",
                "swim.base": 0,
                "climb.base": 0,
                "burrow.base": 0,
              },
            });
            promises.push(p);
          }
          break;
        }
      }
    }

    await Promise.all(promises);
  }

  /**
   * Collect Data on supplements
   *
   * @internal
   * @param {Array<Item>} items - Array of items to draw supplemental data from
   * @param {Actor} actor - Owner
   */
  static async _createSupplements(items, actor) {
    const allSupplements = new Collection();

    let t0 = performance.now();
    const collect = async (item, classLink, _depth = 0) => {
      const supplements = item.system.links?.supplements ?? [];
      classLink ??= item.system.class;

      // Log slow fetches occasionally
      const t1 = performance.now();
      if (t1 - t0 >= 2_000) {
        console.debug("Fetching", supplements.length, "supplements for", item.name);
        t0 = t1;
      }

      // Collect supplements
      const newItemData = [];
      for (const supplement of supplements) {
        const { uuid } = supplement;
        if (!uuid) continue; // Erroneous supplement data
        const extraItem = await fromUuid(uuid);
        if (!extraItem) {
          // TODO: Display notification instead when this is from UI interaction.
          console.warn("Supplement", uuid, "not found for", item.name, item);
          continue;
        }

        const itemData = game.items.fromCompendium(extraItem, { clearFolder: true });
        foundry.utils.setProperty(itemData, "flags.pf1.source", uuid);

        const old = allSupplements.get(uuid);
        if (old) old.count += 1;
        else {
          allSupplements.set(uuid, { parent: item, item: extraItem, data: itemData, count: 1, classLink, uuid });
          newItemData.push(itemData);
        }
      }

      if (!newItemData.length) return;

      // TODO: Make the limits here configurable?
      if (_depth > 3) {
        return void console.warn("Stopping collecting supplements deeper than 3 layers");
      }
      if (allSupplements.size > 100 && newItemData.length) {
        return void console.warn(`Too many supplements (${allSupplements.size}), stopping collecting more`);
      }

      for (const newItem of newItemData) {
        // TODO: Somehow add child relation to the children
        await collect(newItem, classLink, _depth + 1);
      }
    };

    // Collect supplements for all items
    for (const item of items) {
      await collect(item);
    }

    // Do additional preparation on collected data
    const lastSortOrder = {};

    const createData = [];
    for (const supplement of allSupplements) {
      const { item, data: itemData, count } = supplement;

      // Adjust quantity of physical items if more than one was added of the same item
      if (item.isPhysical && itemData.system.quantity > 0) {
        itemData.system.quantity *= count;
      }
      // Inherit class link
      if (supplement.classLink && item.type === "feat" && item.system.subType === "classFeat") {
        itemData.system.class = supplement.classLink;
      }

      // Sort to end of the item list of their type
      const oldItemsOfSameType = actor.itemTypes[itemData.type] ?? [];
      lastSortOrder[itemData.type] ??= oldItemsOfSameType.length
        ? oldItemsOfSameType.reduce((max, i) => Math.max(i.sort || 0, max), -Number.MIN_SAFE_INTEGER) +
          CONST.SORT_INTEGER_DENSITY
        : 0;
      itemData.sort = lastSortOrder[itemData.type];
      lastSortOrder[itemData.type] += CONST.SORT_INTEGER_DENSITY;

      // Create item
      createData.push(itemData);
    }

    // Create supplements
    const newItems = await actor.createEmbeddedDocuments("Item", createData, {
      _pf1NoSupplements: true,
      render: false, // Following child link creation causes render again
    });

    // Add child links
    const updates = new Collection();
    const allItems = [...items, ...newItems];

    /** @type {Collection<Item>} */
    for (const item of newItems) {
      const source = pf1.utils.internal.uniformUuid(item.getFlag("pf1", "source"));
      const parent = allItems.find((i) =>
        i.system.links?.supplements?.some((si) => pf1.utils.internal.uniformUuid(si.uuid) === source)
      );
      if (!parent) {
        console.warn(item.name, "parent not found!", source, allItems);
        continue;
      }

      let update = updates.get(parent.id);
      if (!update) {
        update = { system: { links: { children: [] } } };
        update._id = parent.id;
        updates.set(parent.id, update);
      }
      update.system.links.children.push({ uuid: item.getRelativeUUID(actor) });
    }

    await actor.updateEmbeddedDocuments("Item", Array.from(updates));
  }

  /**
   * Item name
   *
   * @param {boolean} [forcePlayerPerspective=false] - If true, return value players see.
   * @returns {string} - Item name
   */
  // eslint-disable-next-line no-unused-vars
  getName(forcePlayerPerspective = false) {
    return this.name;
  }

  /**
   * Is the item is fully functional.
   *
   * This returns composite result of if the item is equipped, has quantity, is not disabled, is not out of charges, etc.
   * and is not representative if the item can be set active or not via {@link setActive}.
   *
   * @see {@link activeState}
   *
   * @abstract
   * @type {boolean}
   */
  get isActive() {
    return true;
  }

  /**
   * If the item can be activated via {@link setActive}.
   *
   * {@link isActive} can return variable state independent of the toggle that {@link setActive} controls, this returns .
   *
   * @abstract
   * @type {boolean}
   */
  get activeState() {
    return this.isActive;
  }

  /**
   * Set item's active state.
   *
   * @abstract
   * @param {boolean} active - Active state
   * @param {object} [context] - Optional update context
   * @returns {Promise<this>} - Update promise if item type supports the operation.
   * @throws {Error} - If item does not support the operation.
   */
  // eslint-disable-next-line no-unused-vars
  async setActive(active, context) {
    throw new Error(`Item type ${this.type} does not support ItemBasePF#setActive`);
  }

  /**
   * Is this item usable at base level, disregarding per-action details.
   *
   * @abstract
   * @type {boolean}
   */
  get canUse() {
    return this.isActive;
  }

  /**
   * For supporting actions and changes in fromUuid()
   *
   * @override
   *
   * Synced with Foundry v12.331
   */
  getEmbeddedDocument(embeddedName, id, options) {
    switch (embeddedName) {
      case "Action":
        return this.actions?.get(id);
      case "Change":
        return this.changes?.get(id);
    }
    return super.getEmbeddedDocument(embeddedName, id, options);
  }
}
