/**
 * Token Document extension
 */
export class TokenDocumentPF extends TokenDocument {
  /**
   * @internal
   * @override
   * @param {object} data - Creation data
   * @param {object} context - Context
   * @param {User} user - Triggering user
   */
  async _preCreate(data, context, user) {
    await super._preCreate(data, context, user);

    this._preCreateSetSize();
  }

  /**
   * @internal
   * @override
   * @param {object} changed - Changed data
   * @param {object} context - Context
   * @param {User} user - Triggering user
   */
  async _preUpdate(changed, context, user) {
    await super._preUpdate(changed, context, user);

    if (context.recursive === false) return;

    const flags = changed.flags?.pf1;
    if (flags) {
      // Delete flags instead of turning them false
      const deleteFlags = ["staticSize", "disableLowLight", "customVisionRules"];
      for (const flag of deleteFlags) {
        if (flags[flag] === false) {
          flags[`-=${flag}`] = null;
          delete flags[flag];
        }
      }
    }
  }

  /**
   * Handle actor size during token creation.
   */
  _preCreateSetSize() {
    if (!this.actor) return;

    // Apply token size
    if (this.getFlag("pf1", "staticSize")) return;
    const sizeConf = pf1.config.tokenSizes[this.actor.system.traits?.size];
    if (!sizeConf) return;

    this.updateSource({
      width: sizeConf.w,
      height: sizeConf.h,
      texture: {
        scaleX: sizeConf.scale * this.actor.prototypeToken.texture.scaleX,
        scaleY: sizeConf.scale * this.actor.prototypeToken.texture.scaleY,
      },
    });
  }

  /**
   * Called by getTrackedAttributes() when not dealing with data models.
   *
   * @override
   */
  static _getTrackedAttributesFromObject(data, path = []) {
    if (path[0] === "conditions") return { bar: [], value: [] };

    const attr = super._getTrackedAttributesFromObject(data, path);

    if (path[0] === "attributes") {
      if (path.length === 1) {
        // These are necessary for token config inclusion which does not delve into hp, vigor or wounds for some reason
        // This has no effect on combat tracker resources for some reason
        attr.value.push(
          ["attributes", "hp", "value"],
          ["attributes", "hp", "offset"],
          ["attributes", "hp", "max"],
          ["attributes", "hp", "temp"],
          ["attributes", "hp", "nonlethal"],
          ["attributes", "vigor", "value"],
          ["attributes", "vigor", "offset"],
          ["attributes", "vigor", "max"],
          ["attributes", "vigor", "temp"],
          ["attributes", "wounds", "value"],
          ["attributes", "wounds", "offset"],
          ["attributes", "wounds", "max"]
        );
      }

      const hpPart = path[1];
      // Needed by combat tracker and token resources for consistent availability of bars
      if (path.length === 2 && ["hp", "wounds", "vigor"].includes(hpPart)) {
        attr.value.push(
          ["attributes", hpPart, "value"],
          ["attributes", hpPart, "max"],
          ["attributes", hpPart, "offset"]
        );
        attr.bar.push(["attributes", hpPart]);
      }
    }

    // If data is missing entirely, AC will not be listed but the others will
    if (!data) {
      attr.value.push(["attributes", "ac", "normal", "total"]);
    }

    return attr;
  }

  /**
   * Hijack Token health bar rendering to include temporary and temp-max health in the bar display
   *
   * @override
   * @param {string} barName - Bar name
   * @param {object} [options] - Additional options
   * @param {string} [options.alternative] - Alt bar path to use
   * @returns {object|null} - Attribute to be displayed if any
   * Synced with Foundry v12.331
   */
  getBarAttribute(barName, { alternative = null } = {}) {
    let data;
    try {
      data = super.getBarAttribute(barName, { alternative });
    } catch {
      data = null;
    }

    // Make resources editable
    if (data?.attribute.startsWith("resources.")) data.editable = true;

    const offsetAttributes = ["attributes.hp", "attributes.wounds", "attributes.vigor"];
    if (offsetAttributes.includes(data?.attribute)) {
      // Add temp HP on top
      const temp = foundry.utils.getProperty(this.actor?.system, data.attribute + ".temp") || 0;
      data.value += temp;
      // Allow editing
      data.editable = true;
    }

    return data;
  }

  prepareBaseData() {
    this._syncSenses();

    super.prepareBaseData();
  }

  /**
   * Synchronize senses from actor.
   */
  _syncSenses() {
    if (!this.actorId || !this.actor) return;
    if (!game.settings.get("pf1", "systemVision")) return;
    if (this.getFlag("pf1", "customVisionRules")) return;

    this.detectionModes = [];

    // Get base range from source data
    const baseRange = this._source.sight.range;

    this.sight.visionMode = "basic";

    // Basic detection
    const basicMode = { id: DetectionMode.BASIC_MODE_ID, enabled: true, range: baseRange };
    this.detectionModes.push(basicMode);

    const senses = this.actor?.system?.traits?.senses ?? {};

    const convertDistance = (d) => pf1.utils.convertDistance(d)[0];

    // Darkvision, also includes blindsight until we have a better representation
    const darkvision = senses.dv?.total ?? 0;
    const blindsight = senses.bs?.total ?? 0;
    const blackAndWhite = Math.max(darkvision, blindsight);

    if (blackAndWhite > 0) {
      this.sight.visionMode = "darkvision";
      // Upgrade basic mode range if greater
      basicMode.range = Math.max(baseRange, convertDistance(blackAndWhite));
    }

    // -----------------------

    // See invisibility
    if (senses.si) {
      this.detectionModes.push({ id: "seeInvisibility", enabled: true, range: basicMode.range });
    }

    // True seeing
    const trueseeing = senses.tr?.total ?? 0;
    if (trueseeing > 0) {
      // Add normal vision within range of true seeing
      const trr = convertDistance(trueseeing);
      basicMode.range = Math.max(trr, basicMode.range);
      if (trueseeing > blackAndWhite) this.sight.visionMode = "basic";

      this.detectionModes.push({ id: "seeInvisibility", enabled: true, range: trr, limited: true });
    }

    this.sight.range = Math.max(baseRange, basicMode.range);

    // Tremor sense
    if (senses.ts?.total) {
      this.detectionModes.push({ id: "feelTremor", enabled: true, range: convertDistance(senses.ts?.total) });
    }

    // Lifesense
    if (senses.ls?.total) {
      this.detectionModes.push({ id: "lifesense", enabled: true, range: convertDistance(senses.ls?.total) });
    }

    // Blind sense
    if (senses.bse?.total) {
      this.detectionModes.push({ id: "blindSense", enabled: true, range: convertDistance(senses.bse?.total) });
    }

    // Blind sight
    if (senses.bs?.total) {
      this.detectionModes.push({ id: "blindSight", enabled: true, range: convertDistance(senses.bs?.total) });
    }

    // Sort detection modes
    this.detectionModes.sort(this.constructor._sortDetectionModes);

    // Update vision advanced fields with current mode's defaults
    const visionDefaults = CONFIG.Canvas.visionModes[this.sight.visionMode]?.vision?.defaults || {};
    for (const fieldName of ["attenuation", "brightness", "saturation", "contrast"]) {
      if (fieldName in visionDefaults) {
        this.sight[fieldName] = visionDefaults[fieldName];
      }
    }
  }

  static _sortDetectionModes(a, b) {
    if (a.id === DetectionMode.BASIC_MODE_ID) return -1;
    if (b.id === DetectionMode.BASIC_MODE_ID) return 1;

    const src = { a: CONFIG.Canvas.detectionModes[a.id], b: CONFIG.Canvas.detectionModes[b.id] };
    return (src.a?.constructor.PRIORITY ?? 0) - (src.b?.constructor.PRIORITY ?? 0);
  }
}
