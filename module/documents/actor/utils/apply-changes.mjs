import { RollPF } from "@dice/roll.mjs";

/**
 * @param {pf1.documents.actor.ActorPF} actor - Actor to apply changes to
 * @param {object} [options] - Additional options
 * @param {boolean} [options.simple] - Process only simple changes
 */
export const applyChanges = (actor, { simple = false } = {}) => {
  actor.changeOverrides = {};
  /** @type {ItemChange[]} */
  const changes = Array.from(actor.changes).filter((change) => {
    const changeTargetData = pf1.config.buffTargets[change.target];
    return !!changeTargetData?.simple === simple;
  });

  const { targets, types } = getSortChangePriority(actor);
  const _sortChanges = (a, b) => {
    const targetA = targets.indexOf(a.target);
    const targetB = targets.indexOf(b.target);
    const typeA = types.indexOf(a.type);
    const typeB = types.indexOf(b.type);
    const prioA = a.priority ?? 0;
    const prioB = b.priority ?? 0;

    // TODO: Improve ordering logic
    // Currently priority overrides all other considerations, making priority become its own separate processing queue
    // Any alteration of this can however completely break how users use changes.
    return prioB - prioA || targetA - targetB || typeA - typeB;
  };

  // Organize changes by priority
  changes.sort((a, b) => _sortChanges(a, b));

  actor.changeFlags.immuneToMorale = actor.system.traits?.ci?.total?.has("moraleEffects") || false;

  // Get items with change flags
  const chflagItems = actor.items.filter((i) => i.isActive && i.hasChanges && i.system.changeFlags);

  // Parse change flags
  for (const i of chflagItems) {
    if (!i.system.changeFlags) continue;
    for (const [k, v] of Object.entries(i.system.changeFlags)) {
      if (v !== true) continue;
      actor.changeFlags[k] = true;
      if (k !== "loseDexToAC") continue;

      for (const k2 of ["normal", "touch"]) {
        getSourceInfo(actor.sourceInfo, `system.attributes.ac.${k2}.total`).negative.push({
          value: game.i18n.localize("PF1.ChangeFlags.LoseDexToAC"),
          name: i.name,
          type: i.type,
        });
      }
      getSourceInfo(actor.sourceInfo, "system.attributes.cmd.total").negative.push({
        value: game.i18n.localize("PF1.ChangeFlags.LoseDexToAC"),
        name: i.name,
        type: i.type,
      });
    }
  }
  if (!simple) actor.refreshDerivedData();

  // Determine continuous changes
  const continuousChanges = changes.filter((o) => o.continuous === true);

  // Apply all changes
  for (const change of changes) {
    if (actor.changeFlags.immuneToMorale && change.type === "morale") continue;

    const flats = change.getTargets(actor);
    for (const f of flats) {
      actor.changeOverrides[f] ??= createOverride();
    }

    change._safeApplyChange(actor, flats, { applySourceInfo: false });

    // Apply continuous changes
    for (const cc of continuousChanges) {
      if (cc === change) continue;
      const flats = cc.getTargets(actor);
      for (const f of flats) {
        if (!actor.changeOverrides[f]) actor.changeOverrides[f] = createOverride();
      }

      cc._safeApplyChange(actor, flats, { applySourceInfo: false });
    }

    if (!simple) actor.refreshDerivedData();
  }

  // Apply source info for changes
  for (const change of changes) {
    change.applySourceInfo(actor);
  }
};

/**
 * Create override structure
 *
 * @returns {object} - Override base structure
 */
function createOverride() {
  const result = {
    add: {},
    set: {},
  };

  for (const k of Object.keys(pf1.config.bonusTypes)) {
    result.add[k] = null;
    result.set[k] = null;
  }

  return result;
}

const getSortChangePriority = (actor) => {
  const skillBasePrio = pf1.config.buffTargets.skills.sort + 100;
  /** @type {[string, {sort: number}][]} */
  const skillTargets = actor._skillTargets.map((target, index) => [target, { sort: skillBasePrio + index * 10 }]);
  const buffTargets = Object.entries(pf1.config.buffTargets);
  const targets = [...skillTargets, ...buffTargets]
    .sort(([, { sort: aSort }], [, { sort: bSort }]) => aSort - bSort)
    .map(([target]) => target);

  return {
    targets,
    types: Object.keys(pf1.config.bonusTypes),
  };
};

/**
 * @param {Actor} actor - Actor to get data from
 * @param {BuffTarget} target Target (e.g. "ac" or "skills")
 * @param {ModifierType} modifierType Type (e.g. "profane", "untyped", or "dodge"). If undefined, all valid targets will be returned.
 * @param {number} [value]  Value, if known
 * @returns {Array<string>} Array of target paths to modify
 */
export function getChangeFlat(actor, target, modifierType, value) {
  if (target == null) return [];

  const system = actor.system;
  /** @type {string[]} */
  const result = [];

  switch (target) {
    case "mhp":
      result.push("system.attributes.hp.max");
      break;
    case "wounds":
      result.push("system.attributes.wounds.max");
      break;
    case "woundThreshold":
      result.push("system.attributes.wounds.threshold");
      break;
    case "vigor":
      result.push("system.attributes.vigor.max");
      break;
    case "str":
    case "dex":
    case "con":
    case "int":
    case "wis":
    case "cha":
      if (["base", "untypedPerm"].includes(modifierType)) {
        result.push(`system.abilities.${target}.base`);
      }
      result.push(`system.abilities.${target}.total`, `system.abilities.${target}.undrained`);
      break;
    case "strPen":
    case "dexPen":
    case "conPen":
    case "intPen":
    case "wisPen":
    case "chaPen": {
      const ablKey = target.slice(0, -3);
      result.push(`system.abilities.${ablKey}.penalty`);
      break;
    }
    case "strMod":
    case "dexMod":
    case "conMod":
    case "intMod":
    case "wisMod":
    case "chaMod":
      result.push(`system.abilities.${target.slice(0, 3)}.mod`);
      break;
    case "carryStr":
      result.push("system.details.carryCapacity.bonus.total");
      break;
    case "carryMult":
      result.push("system.details.carryCapacity.multiplier.total");
      break;
    case "size":
      result.push("system.traits.size.value");
      break;
    case "ageCategory":
      result.push("system.traits.ageCategory.value");
      result.push("system.traits.ageCategory.mental");
      result.push("system.traits.ageCategory.physical");
      break;
    case "ageCategoryMental":
      result.push("system.traits.ageCategory.mental");
      break;
    case "ageCategoryPhysical":
      result.push("system.traits.ageCategory.physical");
      break;
    case "ac":
      result.push("system.attributes.ac.normal.total", "system.attributes.ac.touch.total");

      switch (modifierType) {
        case "dodge":
        case "haste":
          result.push("system.attributes.cmd.total");
          break;
        case "deflection":
        case "circumstance":
        case "insight":
        case "luck":
        case "morale":
        case "profane":
        case "sacred":
          result.push(
            "system.attributes.ac.flatFooted.total",
            "system.attributes.cmd.total",
            "system.attributes.cmd.flatFootedTotal"
          );
          break;
        default:
          result.push("system.attributes.ac.flatFooted.total");
          // Other penalties also apply to CMD, but not bonuses
          if (value < 0) {
            result.push("system.attributes.cmd.total", "system.attributes.cmd.flatFootedTotal");
          }
          break;
      }
      break;
    case "aac": {
      const targets = ["system.ac.normal.total"];
      switch (modifierType) {
        case "base":
          targets.push("system.ac.normal.base");
          break;
        case "enh":
          targets.push("system.ac.normal.enh");
          break;
        default:
          targets.push("system.ac.normal.misc");
          break;
      }
      result.push(...targets);
      break;
    }
    case "sac": {
      const targets = ["system.ac.shield.total"];
      switch (modifierType) {
        case "base":
          targets.push("system.ac.shield.base");
          break;
        case "enh":
          targets.push("system.ac.shield.enh");
          break;
        default:
          targets.push("system.ac.shield.misc");
          break;
      }
      result.push(...targets);
      break;
    }
    case "nac": {
      const targets = ["system.ac.natural.total"];
      switch (modifierType) {
        case "base":
          targets.push("system.ac.natural.base");
          break;
        case "enh":
          targets.push("system.ac.natural.enh");
          break;
        default:
          targets.push("system.ac.natural.misc");
          break;
      }
      result.push(...targets);
      break;
    }
    case "tac":
      result.push("system.attributes.ac.touch.total");
      break;
    case "ffac":
      result.push("system.attributes.ac.flatFooted.total");
      break;
    case "ffcmd":
      result.push("system.attributes.cmd.flatFootedTotal");
      break;
    case "bab":
      result.push("system.attributes.bab.total");
      break;
    case "~attackCore":
      result.push("system.attributes.attack.shared");
      break;
    case "attack":
      result.push("system.attributes.attack.general");
      break;
    case "wattack":
      result.push("system.attributes.attack.weapon");
      break;
    case "sattack":
      result.push("system.attributes.attack.spell");
      break;
    case "mattack":
      result.push("system.attributes.attack.melee");
      break;
    case "nattack":
      result.push("system.attributes.attack.natural");
      break;
    case "rattack":
      result.push("system.attributes.attack.ranged");
      break;
    case "tattack":
      result.push("system.attributes.attack.thrown");
      break;
    case "critConfirm":
      result.push("system.attributes.attack.critConfirm");
      break;
    case "allSavingThrows":
      result.push(
        "system.attributes.savingThrows.fort.total",
        "system.attributes.savingThrows.ref.total",
        "system.attributes.savingThrows.will.total"
      );
      break;
    case "fort":
      result.push("system.attributes.savingThrows.fort.total");
      break;
    case "ref":
      result.push("system.attributes.savingThrows.ref.total");
      break;
    case "will":
      result.push("system.attributes.savingThrows.will.total");
      break;
    case "vehicleSave":
      result.push("system.attributes.savingThrows.save.total");
      break;
    case "skills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const b of Object.keys(skl.subSkills)) {
            result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "unskills":
      // Untrained skills
      for (const [skillId, skill] of Object.entries(system.skills)) {
        if (skill == null) continue;
        for (const [subSkillId, subskill] of Object.entries(skill.subSkills ?? {})) {
          if (subskill.rank > 0) continue;
          result.push(`system.skills.${skillId}.subSkills.${subSkillId}.mod`);
        }
        if (skill.rank > 0) continue;
        result.push(`system.skills.${skillId}.mod`);
      }
      break;
    case "reach":
      // Natural reach
      result.push("system.traits.reach.total.melee");
      result.push("system.traits.reach.total.reach");
      break;
    case "strSkills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        if (skl.ability === "str") result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const [b, subSkl] of Object.entries(skl.subSkills)) {
            if (subSkl != null && subSkl.ability === "str") result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "dexSkills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        if (skl.ability === "dex") result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const [b, subSkl] of Object.entries(skl.subSkills)) {
            if (subSkl != null && subSkl.ability === "dex") result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "conSkills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        if (skl.ability === "con") result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const [b, subSkl] of Object.entries(skl.subSkills)) {
            if (subSkl != null && subSkl.ability === "con") result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "intSkills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        if (skl.ability === "int") result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const [b, subSkl] of Object.entries(skl.subSkills)) {
            if (subSkl != null && subSkl.ability === "int") result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "wisSkills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        if (skl.ability === "wis") result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const [b, subSkl] of Object.entries(skl.subSkills)) {
            if (subSkl != null && subSkl.ability === "wis") result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "chaSkills":
      for (const [a, skl] of Object.entries(system.skills)) {
        if (skl == null) continue;
        if (skl.ability === "cha") result.push(`system.skills.${a}.mod`);

        if (skl.subSkills != null) {
          for (const [b, subSkl] of Object.entries(skl.subSkills)) {
            if (subSkl != null && subSkl.ability === "cha") result.push(`system.skills.${a}.subSkills.${b}.mod`);
          }
        }
      }
      break;
    case "allChecks":
      result.push(
        "system.abilities.str.checkMod",
        "system.abilities.dex.checkMod",
        "system.abilities.con.checkMod",
        "system.abilities.int.checkMod",
        "system.abilities.wis.checkMod",
        "system.abilities.cha.checkMod",
        ...(system.attributes.init.ability ? ["system.attributes.init.total"] : [])
      );
      break;
    case "strChecks":
      result.push(
        "system.abilities.str.checkMod",
        ...(system.attributes.init.ability === "str" ? ["system.attributes.init.total"] : [])
      );
      break;
    case "dexChecks":
      result.push(
        "system.abilities.dex.checkMod",
        ...(system.attributes.init.ability === "dex" ? ["system.attributes.init.total"] : [])
      );
      break;
    case "conChecks":
      result.push(
        "system.abilities.con.checkMod",
        ...(system.attributes.init.ability === "con" ? ["system.attributes.init.total"] : [])
      );
      break;
    case "intChecks":
      result.push(
        "system.abilities.int.checkMod",
        ...(system.attributes.init.ability === "int" ? ["system.attributes.init.total"] : [])
      );
      break;
    case "wisChecks":
      result.push(
        "system.abilities.wis.checkMod",
        ...(system.attributes.init.ability === "wis" ? ["system.attributes.init.total"] : [])
      );
      break;
    case "chaChecks":
      result.push(
        "system.abilities.cha.checkMod",
        ...(system.attributes.init.ability === "cha" ? ["system.attributes.init.total"] : [])
      );
      break;
    case "allSpeeds":
      for (const speedKey of Object.keys(system.attributes.speed)) {
        result.push(`system.attributes.speed.${speedKey}.total`);
      }
      break;
    case "landSpeed":
      result.push("system.attributes.speed.land.total");
      break;
    case "climbSpeed":
      result.push("system.attributes.speed.climb.total");
      break;
    case "swimSpeed":
      result.push("system.attributes.speed.swim.total");
      break;
    case "burrowSpeed":
      result.push("system.attributes.speed.burrow.total");
      break;
    case "flySpeed":
      result.push("system.attributes.speed.fly.total");
      break;
    case "cmb":
      result.push("system.attributes.cmb.bonus");
      break;
    case "cmd":
      if (["dodge", "haste"].includes(modifierType)) {
        result.push("system.attributes.cmd.total");
        break;
      }
      result.push("system.attributes.cmd.total", "system.attributes.cmd.flatFootedTotal");
      break;
    case "init":
      result.push("system.attributes.init.total");
      break;
    case "acpA":
      result.push("system.attributes.acp.armorBonus");
      break;
    case "acpS":
      result.push("system.attributes.acp.shieldBonus");
      break;
    case "mDexA":
      result.push("system.attributes.mDex.armorBonus");
      break;
    case "mDexS":
      result.push("system.attributes.mDex.shieldBonus");
      break;
    case "spellResist":
      result.push("system.attributes.sr.total");
      break;
    case "damage":
      result.push("system.attributes.damage.general");
      break;
    case "mdamage":
      result.push("system.attributes.damage.meleeAll");
      break;
    case "rdamage":
      result.push("system.attributes.damage.rangedAll");
      break;
    case "wdamage":
      result.push("system.attributes.damage.weapon");
      break;
    case "rwdamage":
      result.push("system.attributes.damage.ranged");
      break;
    case "twdamage":
      result.push("system.attributes.damage.thrown");
      break;
    case "mwdamage":
      result.push("system.attributes.damage.melee");
      break;
    case "ndamage":
      result.push("system.attributes.damage.natural");
      break;
    case "sdamage":
      result.push("system.attributes.damage.spell");
      break;
    case "bonusFeats":
      result.push("system.details.feats.bonus");
      break;
    case "bonusSkillRanks":
      result.push("system.details.skills.bonus");
      break;
    case "concentration":
      result.push(
        "system.attributes.spells.spellbooks.primary.concentration.total",
        "system.attributes.spells.spellbooks.secondary.concentration.total",
        "system.attributes.spells.spellbooks.tertiary.concentration.total",
        "system.attributes.spells.spellbooks.spelllike.concentration.total"
      );
      break;
    case "cl":
      result.push(
        "system.attributes.spells.spellbooks.primary.cl.total",
        "system.attributes.spells.spellbooks.secondary.cl.total",
        "system.attributes.spells.spellbooks.tertiary.cl.total",
        "system.attributes.spells.spellbooks.spelllike.cl.total"
      );
      break;
    case "dc":
      result.push(`system.attributes.spells.school.all.dc`);
      break;
    case "sensedv":
      result.push("system.traits.senses.dv.total");
      break;
    case "sensets":
      result.push("system.traits.senses.ts.total");
      break;
    case "sensebse":
      result.push("system.traits.senses.bse.total");
      break;
    case "sensebs":
      result.push("system.traits.senses.bs.total");
      break;
    case "sensels":
      result.push("system.traits.senses.ls.total");
      break;
    case "sensetr":
      result.push("system.traits.senses.tr.total");
      break;
    case "sensesc":
      result.push("system.traits.senses.sc.total");
      break;
  }

  // Per school DC target
  const schoolDC = /^dc\.school\.(?<schoolId>\w+)/.exec(target);
  if (schoolDC) {
    const schoolId = schoolDC.groups.schoolId;
    result.push(`system.attributes.spells.school.${schoolId}.dc`);
  }

  // Per school CL target
  const schoolCL = /^cl\.school\.(?<schoolId>\w+)/.exec(target);
  if (schoolCL) {
    const schoolId = schoolCL.groups.schoolId;
    result.push(`system.attributes.spells.school.${schoolId}.cl`);
  }

  // Per book concentration target
  const concnMatch = /^concn\.(?<bookId>\w+)/.exec(target);
  if (concnMatch) {
    const bookId = concnMatch.groups.bookId;
    result.push(`system.attributes.spells.spellbooks.${bookId}.concentration.total`);
  }

  // Per book caster level target
  const bookCL = /^cl\.book\.(?<bookId>\w+)/.exec(target);
  if (bookCL) {
    const bookId = bookCL.groups.bookId;
    result.push(`system.attributes.spells.spellbooks.${bookId}.cl.bonus`);
  }

  if (/^skill\./.test(target)) {
    const parts = target.split(".").slice(1);
    let sklKey = parts.shift();

    let spread = true;
    if (sklKey[0] === "~") {
      spread = false;
      sklKey = sklKey.slice(1);
    }

    const skillData = system.skills[sklKey];
    if (skillData) {
      const subSklKey = parts.pop();

      if (subSklKey) {
        // Apply to all skills in this tree, including parent
        if (skillData.subSkills?.[subSklKey]) {
          result.push(`system.skills.${sklKey}.subSkills.${subSklKey}.mod`);
        }
      } else {
        result.push(`system.skills.${sklKey}.mod`);
        if (spread) {
          for (const subSklKey of Object.keys(skillData.subSkills ?? {})) {
            result.push(`system.skills.${sklKey}.subSkills.${subSklKey}.mod`);
          }
        }
      }
    }
  }

  // Call hooks to enable modules to add or adjust the result array
  if (Hooks.events.pf1GetChangeFlat?.length) {
    Hooks.callAll("pf1GetChangeFlat", result, target, modifierType, value, this);
  }

  // Return results directly when deprecation is removed
  return result;
}

/**
 * @deprecated - Implementation moved to {@link pf1.documents.actor.abstract.BaseCharacterPF#_prepareTypeChanges}
 * @param {*} actor
 * @param {*} changes
 */
export function addDefaultChanges(actor, changes) {
  foundry.utils.logCompatibilityWarning(
    "pf1.documents.actor.changes.addDefaultChanges() is deprecated in favor of pf1.documents.actor.abstract.BaseCharacterPF#_prepareTypeChanges()",
    {
      since: "PF1 v11",
      until: "PF1 v12",
    }
  );
}

/**
 * Initialize source info structure and retrieve it
 *
 * @param {object} obj  - Source info base structure
 * @param {string} key - Data path
 * @returns {object} - Target source info structure
 */
export function getSourceInfo(obj, key) {
  obj[key] ??= { negative: [], positive: [] };
  return obj[key];
}

/**
 * @param {object} obj - Source info base
 * @param {string} key - Data path
 * @param {string} name - Name to find
 * @param {*} value - Value to set
 * @param {boolean} [positive=true] - Is this positive or negative source
 * @param {string} [id] - Special ID
 */
export function setSourceInfoByName(obj, key, name, value, positive = true, id) {
  const target = positive ? "positive" : "negative";
  const sourceInfo = getSourceInfo(obj, key)[target];
  const data = sourceInfo.find((o) => o.name === name);
  if (data) data.value = value;
  else {
    const srcInfo = {
      name,
      value,
    };
    if (id) srcInfo.id = id;
    sourceInfo.push(srcInfo);
  }
}

/**
 * @param {ItemChange[]} changes - An array containing all changes to check. Must be called after they received a value (by ItemChange.applyChange)
 * @param {object} [options] - Additional options
 * @param {boolean} [options.ignoreTarget] - Whether to only check for modifiers such as enhancement, insight (true) or whether the target (AC, weapon damage) is also important (false)
 * @returns {ItemChange[]} - A list of processed changes, excluding the lower-valued ones inserted (if they don't stack)
 */
export function getHighestChanges(changes, options = { ignoreTarget: false }) {
  const highestTemplate = {
    value: 0,
    ids: [],
    highestID: null,
  };
  const highest = Object.keys(pf1.config.bonusTypes).reduce((cur, k) => {
    if (options.ignoreTarget) cur[k] = foundry.utils.deepClone(highestTemplate);
    else cur[k] = {};
    return cur;
  }, {});

  for (const c of changes) {
    let h;
    if (options.ignoreTarget) h = highest[c.type];
    else h = highest[c.type]?.[c.target];

    if (!h) continue; // Ignore bad changes
    h.ids.push(c._id);
    if (h.value < c.value || !h.highestID) {
      h.value = c.value;
      h.highestID = c._id;
    }
  }

  let type, h;
  const filterFunc = (c) => {
    if (h.highestID === c._id) return true;
    if (pf1.config.stackingBonusTypes.indexOf(type) === -1 && h.ids.includes(c._id)) return false;
    return true;
  };

  for (type of Object.keys(highest)) {
    if (options.ignoreTarget) {
      h = highest[type];
      changes = changes.filter(filterFunc);
    } else {
      for (const subTarget of Object.keys(highest[type])) {
        h = highest[type][subTarget];
        changes = changes.filter(filterFunc);
      }
    }
  }

  return changes;
}
